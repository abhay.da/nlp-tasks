This branch contains the dockerized code for intent overlap detection.

> The "**./flask-docker/Code/**" directory contains the main code for the API and the script for intent overlap detection.

> The "**./flask-docker/Test/**" directory contains code to test the API.

**How to run** :

Make sure you are in the **flask-docker** directory.

Building Image:

* docker build -t flask-api:1.0 .

Running the container : 

* docker run -d -p 5000:8080 flask-api:1.0


Testing the API:

* cd Test 

* python test.py

