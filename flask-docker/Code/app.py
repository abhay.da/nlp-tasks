import intents
from flask import Flask, request
from flask_restful import Api, Resource
import warnings

app = Flask(__name__)
api = Api(app)

class GetOverlappingIntents(Resource):
    
    def get(self):
        return "Application is live!"

    def put(self):
        
        '''
        The json payload of the incoming HTTP PUT request should be in the following format:
        
        Example:
        {
            'version': '3.0',
            'nlu': [ 
                        {'intent': 'outofscope', 'examples': '-the weather is good\n'}, 
                        {'intent': 'intent_repeat', 'examples': '- hello\n- hello hello\n- what\n- hello madam\n- what what\n- what to say again\n- i did not understand\n'},
                        {'intent': 'intent_busy', 'examples' : '- do not call at this time\n- i cant talk to you right now\n- can we have this conversation later\n- busy\n- call me later\n"}, 
                        {'intent': 'intent_affirm', 'examples': '- i know\n- you are correct\n- yes sure\n- yes please go ahead\n- yes please go on\nyes positively'}, 
                        {'intent': 'intent_deny', 'examples': "- no\n- dont want\n- do not do that\n- no it is not\n- no you cant\n- unsure about that\n- not sure\n},
                        {...}
                        .
                        .
                        ...so on
                    ]
        }
        NOTE:   This is the format that is obtained when the nlu.yaml file is parsed by the yaml parser in python.
                The 'nlu' key is very important for the request to be successful.

        Returns
        -------
        response : dictionary / json object
            Returns a response object which has the overlapping intents and the graph object if the API call was successful, else returns an error.
        '''

        threshold = 0.7
        n_neighbors = 45

        try : 

            if request.args:

                threshold = float(request.args['threshold'])
                n_neighbors = int(float(request.args['n_neighbours']))

                if not 0.0 < threshold < 1.0:
                    warnings.warn("Bad threshold value. Expected value between (0, 1). Using default value of 0.7.", category=Warning)
                    threshold = 0.7

                if n_neighbors < 0:
                    warnings.warn("Bad n_neighbours value. Expected a value greater than 0. Using default value of 45.", category=Warning)
                    n_neighbors = 45
                
        except Exception as ex:
            return {"error" : str(ex)}, 400

        print(threshold)
        print(n_neighbors)

        try:
            if request.is_json:

                data = request.json
                obj = intents.overlappingIntents(n_neighbors = n_neighbors)
                result, graph, html = obj.getOverLappingIntents(intents=data, threshold=threshold)
                
                response = {"result" : result, "graph" : graph, "html" : html}
            else:
                return {"error" : "Add a json payload"}, 400

        except Exception as e:
            return {"error" : str(e)}, 400

        return response

api.add_resource(GetOverlappingIntents, '/')

if __name__ == '__main__':
    app.run(
        port = 8080,
        host = "0.0.0.0"
    )
