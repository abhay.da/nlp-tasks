#!/usr/bin/env python

import warnings
import re
import numpy as np
import pandas as pd

from nltk.tokenize import word_tokenize

from sentence_transformers import SentenceTransformer, util

import inflect
import umap
import plotly.express as px
import time
import json


class overlappingIntents:
    
    def __init__(self, n_neighbors = 45, path = '/application/Code/Models/', contraction_file = '/application/Code/contractions.json') -> None:
        '''
        Parameters
        ----------
        n_neighbors: int
            Configurable parameter. (To be chosen with caution as the plot can vary depending on the value of this parameter)

        path: 
            Specifies the location of the downloaded pre-trained model.
        '''

        print("Loading Models")

        # Pre-trined model used : 'sentence-transformers/stsb-roberta-base-v2'
        self.__model = SentenceTransformer(path)

        self.__n_components = 2    # Final dimension of the embeddings vector, i.e, 768 -> 2
        self.__n_neighbors = n_neighbors    
        self.__reducer = umap.UMAP(random_state = 42, n_neighbors = self.__n_neighbors, min_dist=0.0, n_components=self.__n_components)

        with open(contraction_file, 'r') as d:
            self.__contractions = json.load(d)
        

    def __expandContractions(self, phrase) -> str:
        '''
        Function to expand english contractions.
        Eg : won't -> will not
             dont -> do not
             you're -> you are
             that's -> that is

        Parameters
        ----------
        phrase : str
            Takes in a string argument.

        Returns
        -------
        phrase : str
            Returns a string with all the contractions expanded, if any.
        '''

        ditionary = self.__contractions
        for key in ditionary:
            phrase = re.sub(r'{regex}'.format(regex = key), ditionary[key], phrase)

        return phrase

    def __convertNumToText(self, text) -> str:
        '''
        Function to convert numbers to its english form.
        Eg: 23 -> twenty three
            1234 -> one thousand, two hundred and thirty-four
        
        Parameters
        ----------
        text: str
            Takes in a string argument.

        Returns
        -------
        text: str
            Returns a string with all the numbers converted to its english form, if any.

        '''
        nums = re.findall(r'\d+', text)
        if nums:
            for num in nums:
                text = re.sub(num, inflect.engine().number_to_words(int(num)), text)
            text = re.sub('-', ' ', text)
            
        return text

    def __preprocess(self, text : str) -> str:
        '''
        Function to perform text preprocessing.

        Parameters
        ----------
        text: str
            Takes in a string argument.
        Returns
        -------
        text: str
            Returns a string with the preprocessing done.
        '''


        # The words mentioned in this list will be removed from the string if present. 
        # List can be expanded to add more stopwords.
        stop_words = ['i', 'a', 'an']   

        text = text.strip().lower()
        text = re.sub('\{(.*?)\}', '', text)    # Removes everything that is wrapped by flower brackets.
        text = re.sub('\]\(', ' ', text)
        text = re.sub('[-?\(\)\[\]\$\}\{\.<>]', '', text)   # removes 
        text = re.sub(r"(?<=\d)(st|nd|rd|th)\b", '', text)
        text = self.__expandContractions(text)
        text = ' '.join([w for w in word_tokenize(text) if not w in stop_words])
        text = self.__convertNumToText(text)

        return text

    def __cleanIntents(self, intents):

        '''
        Function takes in a list of dictionaries.
        Each dictionary has the intent and the utterances belonging to that intent.
        The utterances will be available in the "examples" key and will be in a raw string format.
        This function converts this raw string to a list of utterances.

        Eg : 
        INPUT -> [{'intent': 'greet', 'examples' : '- hi\n- hello\n- good morning\n' }, {...}]
        RETURNS -> [{'intent': 'greet', 'examples' : ['hi', 'hello', 'good morning'] }, {...}]

        Parameters
        ----------
        intents: list of dictionaries
            Takes in a list of dictionaries.

        Returns
        -------
        intents: list of dictionaries
            Returns a list of dictionaries
        '''

        for intent in intents:
            
            if intent['examples'] == '':
                warnings.warn('Empty Intent Detected. Skipping intent \"{intent}\". '.format(intent = intent['intent']), category= Warning)
            else:
                intent['examples'] = re.sub('[-?\[\]]', '', intent['examples'])
                intent['examples'] = re.sub('\n', ',', intent['examples'])
                intent['examples'] = re.sub('\((.*?)\)', '', intent['examples'])    # Removes slots

                intent['examples'] = intent['examples'].strip().split(',')[:-1]
                intent['examples'] = list(map(lambda x: x.strip().lower(), intent['examples']))
        
        return intents


    def __getPlotJson(self, df, embeddings):
        '''
        This function takes the sentence embeddings and returns an interactive 2D plot of the same for visualisation.
        Note :  Here, UMAP is used only to aid visualisation. 
                The resuts of UMAP are NOT used in downstream tasks. 
                The polt is only an approximate visualisation of the embedding space.

        Parameters
        ----------
        df : pandas dataframe
            Dataframe where each row contains the intent and the corresponding utterance.

        embeddings : tensor
            2-D tensor containing the sentence embeddings of the utterances.

        Returns
        -------
        graph_json : json object
            Returns a json object of 2D plot generated by plotly.

        '''
        graph_json, graph_html = None, None

        roberta_umap_emb = self.__reducer.fit_transform(embeddings)

        fig = px.scatter(df, x = roberta_umap_emb[:,0], y = roberta_umap_emb[:,1], color= 'intent', hover_data= ['clean_text'], title = "Intent Plot")
        
        graph_json = fig.to_json()  # Converts figure to json string

        graph_html = fig.to_html()  # Converts figure to html string
        
        return graph_json, graph_html

    def __makeDF(self, intents):
        '''
        
        Function to make a dataframe from the intents.
        
        Eg : 
        INPUT -> [{'intent': 'greet', 'examples' : ['hi', 'hello', 'good morning'] }, {...}]
        
        RETURNS -> 

        | intent |   sentence   | clean_text   |
        ----------------------------------------
        | greet  |       hi     |     hi       |
        | greet  |     hello    |   hello      |
        | greet  | good morning | good morning | 

        Parameters
        ----------
        intents : list of dictionaries

        Returns
        -------
        df : pandas dataframe
       
        '''
        df = pd.DataFrame(columns = ['intent', 'sentence'])
        
        for data in intents:
            
            for sentence in data['examples']:
                try:
                    row = {'intent': [data['intent']], 'sentence' : [sentence]}
                    df2 = pd.DataFrame.from_dict(row)
                    df = pd.concat([df, df2],ignore_index = True, axis = 0)
                except KeyError:
                    continue
        
        # Creates a new column containing the cleaned form of the original sentences.
        df['clean_text'] = df['sentence'].apply(lambda x: self.__preprocess(x))     
        
        return df
        
    
    def getOverLappingIntents(self, intents, threshold = 0.7):
        
        '''
        Parameters
        ----------
        intents : dictionary
            Raw form of the nlu data after being parsed.

        threshold : float
            This value specifies the threshold of similarity between two utterances.
            If two sentences have a similarity score of higher than the threshold, it is considered as overlapping.

        Returns
        -------
        result : list of dictionaries
            This object contains the overlapping intents as shown in the below example.
            Eg: [
                    {
                        "sentence": "hello hello",
                        "actual_intent": "intent_repeat",
                        "overlapping": [
                            {
                                "intent": "intent_affirm",
                                "sentence": "yeah hello"
                            },
                            {
                                "intent": "intent_affirm",
                                "sentence": "hello yeah yeah"
                            },
                            {
                                "intent": "intent_affirm",
                                "sentence": "hello yeah"
                            },
                            {
                                "intent": "intent_affirm",
                                "sentence": "yeah hello yeah"
                            },
                            {
                                "intent": "intent_affirm",
                                "sentence": "yeah hello yes"
                            }
                        ]
                    },
                    {
                        .......
                    },
                ]

        graph_json : json objet
            Returns a json object of an interactive 2D plot generated by plotly.
        '''

        try:
            intents = self.__cleanIntents(intents['nlu'])
        except TypeError:
            raise Exception('nlu tag might be missing in the input file.')
        except Exception as ex:
            raise ex

        result = []

        df = self.__makeDF(intents)
        
        if df.empty:
            raise Exception("No utterances were detected.")

        sentences = df.clean_text.to_list()
        
        # getting sentence embeddings
        embeddings = self.__model.encode(sentences, convert_to_tensor=True, batch_size = 64, show_progress_bar = True)
        
        cosine_scores = util.cos_sim(embeddings, embeddings)

        cosine_scores = cosine_scores.numpy()
        cosine_scores = np.where(cosine_scores <= 0.0, 0, cosine_scores)
        cosine_scores = np.where(cosine_scores >= 1.0, 1, cosine_scores)
        np.fill_diagonal(cosine_scores, 0.0)

        cosine_scores = np.triu(cosine_scores)     # taking only the upper triangular similarity matrix to avoid repetition in printing overlapped utterances

        intent_sentence = list(zip(df.intent.to_list(), df.sentence.to_list()))

        for i in range(len(cosine_scores)):

            row = cosine_scores[i]

            curr_intent = intent_sentence[i][0]
            curr_sentence = intent_sentence[i][1]

            indices = [x[0] for x in np.argwhere(row > threshold)]
            
            if len(indices) > 0:

                pairs = [intent_sentence[k] for k in indices]

                d = {}

                d['sentence'] = curr_sentence
                d['actual_intent'] = curr_intent

                overlapped_sentences = []

                for pair in pairs:
                    temp = {}
                    if pair[0] != curr_intent:
                        temp = {}
                        temp['intent'] = pair[0]
                        temp['sentence'] = pair[1]
                        overlapped_sentences.append(temp)

                if overlapped_sentences:
                    d['overlapping'] = overlapped_sentences
                    result.append(d)
        
        graph_json, graph_html = self.__getPlotJson(df, embeddings)
        
        return result, graph_json, graph_html