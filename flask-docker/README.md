***Building the image:***

> docker build -t flask-api:1.0 .

***Running the container :***

> docker run -d -p 5000:8080 flask-api:1.0

<br>

***Testing the API:***

> cd Test

> python test.py