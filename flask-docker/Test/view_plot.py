import json
import plotly


with open("plot.json", "r") as open:
	graph_data = json.load(open)


img = plotly.io.from_json(graph_data)
img.show()
