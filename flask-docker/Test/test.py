import json
import yaml
import time
import requests

#path = "./nlu.yml"
path = "./tata_motors.yaml"
#path = "./tata_cliq.yaml"

with open(path, "r") as stream:
    intents = yaml.safe_load(stream)

payload = intents

print(type(payload))
print()

url = 'http://127.0.0.1:5000/'

params = {'threshold' : 0.7, 'n_neighbours' : 45}

print("Hit enter to send request...", end = '')
input()
print("Request Sent")

start_time = time.time()
response = requests.put(url, json = payload, params=params)
end_time = time.time()
print("Time Taken : ", end_time-start_time)

print(response.status_code)
print(dict(response.json()).keys())

if response.status_code == 200:

    with open("results.json", "w") as f:
        json_object = json.dumps(response.json()["result"], indent='\t')
        f.write(json_object)

    with open("plot.json", "w") as fout:
        graph_object = json.dumps(response.json()["graph"])
        fout.write(graph_object)

    if response.json()["html"] != None:
        with open("plot.html", "w") as html:
            html_string = response.json()["html"]
            html.write(html_string)
    else:
        print("No HTML file returned. Uncomment line and rebuild to get an html file")
else : 
    print(response.json()['error'])
    print("Request unsuccessful")



